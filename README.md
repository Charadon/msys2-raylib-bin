Simply use msys2 to git clone this repository, and then run "makepkg-mingw -si" to build it.

Purpose: Integrates raylib into a more unix-like environment on windows. Makes porting software a hell of a lot easier for developers who are mainly using unix-like operating systems.